﻿using UnityEngine;

namespace Utils.Behaviours {

	/// <summary>
	/// Enables you to switch scenes by the press of a key or a button.
	/// If a button name is given the key will be ignored.
	/// If no target scene is set the current scene will be reloaded.
	/// </summary>
	public class SceneLoadController : MonoBehaviour {

		[Tooltip("The key that loads the given scene.\nIf a button name is given the key will be ignored.")]
		/// <summary>
		/// The key.
		/// </summary>
		public KeyCode Key = KeyCode.F5;
		[Tooltip("The name of the button that loads the given scene.\nIf a button name is given the key will be ignored.")]
		/// <summary>
		/// The name of the button.
		/// </summary>
		public string ButtonName;
		[Tooltip("The name of the scene to load.\nIf no target scene is set the current scene will be reloaded.")]
		/// <summary>
		/// The target scene.
		/// </summary>
		public string TargetScene;
		
		/// <summary>
		/// Poll input every update.
		/// </summary>
		void Update () {
			if ((string.IsNullOrEmpty(ButtonName)) ? UnityEngine.Input.GetKeyDown(Key) : UnityEngine.Input.GetButtonDown(ButtonName))
				Application.LoadLevel((string.IsNullOrEmpty(TargetScene)) ? Application.loadedLevelName : TargetScene);
		}
	}
}

﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Utils.Helpers {

	/// <summary>
	/// Enables you to read and write serializable data to a binary file.
	/// </summary>
	public abstract class BinaryFileHelper {
	
		/// <summary>
		/// Writes serializable data to a file.
		/// </summary>
		/// <param name="filePath">The path to the file.</param>
		/// <param name="data">Data.</param>
		public static void WriteToFile(string filePath, object data) {
			// Try to write file
			try {
				using (Stream stream = File.Create(filePath)) {
					BinaryFormatter serializer = new BinaryFormatter();
					serializer.Serialize(stream, data);
				}
			} catch (Exception e) {
				Debug.LogException(e);
			}
		}
	
		/// <summary>
		/// Reads serialized data from a file.
		/// Returns 'null' if the file is not found.
		/// </summary>
		/// <returns>The data inside the file or null.</returns>
		/// <param name="filePath">The path to the file.</param>
		public static object ReadFromFile(string filePath) {
			// Try to read file or return null
			if (File.Exists(filePath)) {
				object data = null;
				
				try {
					using (Stream stream = File.OpenRead(filePath)) {
						BinaryFormatter deserializer = new BinaryFormatter();
						data = deserializer.Deserialize(stream);
					}
				} catch (Exception e) {
					Debug.LogException(e);
				}
				
				return data;
			} else {
				// File not found warning
				Debug.LogWarning("File \"" + filePath + "\" not found! Returning null.");
				return null;
			}
		}
	}
}

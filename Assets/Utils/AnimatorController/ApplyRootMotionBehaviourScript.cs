﻿using UnityEngine;

namespace Utils.AnimatorController {
	
	/// <summary>
	/// Apply root motion behaviour script.
	/// </summary>
	public class ApplyRootMotionBehaviourScript : StateMachineBehaviour {

		[Tooltip("Whether to apply root motion on entering this state or not.")]
		/// <summary>
		/// Determines whether to apply root motion on entering this state or not.
		/// </summary>
		public bool ApplyRootMotionOnEnter = true;
		[Tooltip("Whether to apply root motion on exiting this state or not.")]
		/// <summary>
		/// Determines whether to apply root motion on exiting this state or not.
		/// </summary>
		public bool ApplyRootMotionOnExit = false;

		override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
			animator.applyRootMotion = ApplyRootMotionOnEnter;
		}
		
		override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
			animator.applyRootMotion = ApplyRootMotionOnExit;
		}
	}
}

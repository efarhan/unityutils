using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Rotates.
	/// </summary>
	public class Rotate : Effect {
		
		[Header("Function parameters")]
		[Tooltip("Determies whether to use the curve or the ease function.")]
		/// <summary>
		/// Determies whether to use the curve or the ease function.
		/// </summary>
		public bool UseCurve = false;
		[Tooltip("The curve.")]
		/// <summary>
		/// The curve.
		/// </summary>
		public AnimationCurve Curve;
		[Tooltip("The ease function.")]
		/// <summary>
		/// The ease function.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;
		
		[Header("Effect parameters")]
		[Tooltip("Whether to reset to the original position at the end.")]
		/// <summary>
		/// Whether to reset to the original rotation at the end.
		/// </summary>
		public bool ResetAtTheEnd = false;
		[Tooltip("The rotation axis influence.")]
		/// <summary>
		/// The rotation axis influence.
		/// </summary>
		public Vector3 AxisInfluence;
		[Tooltip("The amount to rotate by (in degrees).")]
		/// <summary>
		/// The amount to rotate by (in degrees).
		/// </summary>
		public float Amount;

		/// <summary>
		/// The original rotation.
		/// </summary>
		private Quaternion originalRotation;
		/// <summary>
		/// The target rotation.
		/// </summary>
		private Quaternion targetRotation;
		
		protected override void InitEffect() {
			// Get original rotation and calculate target rotation
			originalRotation = transform.localRotation;
			targetRotation = originalRotation * Quaternion.Euler(AxisInfluence * Amount);
		}
		
		protected override void UpdateEffect() {
			// Update rotation according to settings
			// TODO: Don't look for shortest way -> allow for multiple rotations
			if (UseCurve) transform.localRotation = Ease.GetEase(Curve, currentTime / Duration, originalRotation, targetRotation);
			else transform.localRotation = Ease.GetEase(EaseFunction, currentTime / Duration, originalRotation, targetRotation);
		}
		
		protected override void CleanUpEffect() {
			// Reset rotation if necessairy
			if (ResetAtTheEnd) transform.localRotation = originalRotation;
		}
	}
}

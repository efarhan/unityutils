using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Hand camera effect.
	/// </summary>
	public class HandCamera : Effect {

		[Header("Effect parameters")]
		[Tooltip("The amount of the effect. Result is also dependent on the given duration (this effect is infinite).")]
		/// <summary>
		/// The amount of the effect. Result is also dependent on the given duration (this effect is infinite).
		/// </summary>
		public float Amount = 1f;
		[Tooltip("The ease function.")]
		/// <summary>
		/// The ease function.
		/// </summary>
		public Ease.EaseFunctions EaseFunction;

		/// <summary>
		/// The local duration for hand shaking.
		/// </summary>
		private float localDuration;
		/// <summary>
		/// The local current time for hand shaking.
		/// </summary>
		private float localCurrentTime;
		/// <summary>
		/// The original position.
		/// </summary>
		private Vector3 originalPosition;
		/// <summary>
		/// The start position.
		/// </summary>
		private Vector3 startPosition;
		/// <summary>
		/// The target position.
		/// </summary>
		private Vector3 targetPosition;

		protected override void InitEffect() {
			infinite = true;

			localDuration = Random.Range(Duration / 2f, Duration);
			localCurrentTime = currentTime;

			// Get and calculate positions
			originalPosition = transform.localPosition;
			startPosition = originalPosition;
			targetPosition = originalPosition + Random.insideUnitSphere * Random.Range(0f, (Amount / 2f));
		}
		
		protected override void UpdateEffect() {
			// Increase local current time
			localCurrentTime += Time.deltaTime;

			// Hand shaking
			transform.localPosition = Ease.GetEase(EaseFunction, localCurrentTime / localDuration, startPosition, targetPosition);
			if (localCurrentTime / localDuration >= 1f) {
				localCurrentTime -= localDuration;
				localDuration = Random.Range(Duration / 2f, localDuration);
				startPosition = targetPosition;
				targetPosition = originalPosition + Random.insideUnitSphere * Random.Range(0f, (Amount / 2f));
			}

			// Breathing
			transform.localPosition = transform.localPosition + Vector3.up * (Amount / 2f) *  Mathf.Sin(currentTime * 2f / Duration);
		}
		
		protected override void CleanUpEffect() {
			// Reset position
			transform.localPosition = originalPosition;
		}
	}
}

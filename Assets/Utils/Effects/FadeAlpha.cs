using UnityEngine;
using Utils;

namespace Utils.Effects {

	/// <summary>
	/// Fades the alpha values of all materials.
	/// </summary>
	public class FadeAlpha : Effect {

		[Header("Effect parameters")]
		[Tooltip("The start alpha value.")]
		/// <summary>
		/// The start alpha value.
		/// </summary>
		public float Start = 0f;
		[Tooltip("The end alpha value.")]
		/// <summary>
		/// The end alpha value.
		/// </summary>
		public float End = 1f;
		[Tooltip("The ease function to use.")]
		/// <summary>
		/// The ease function to use.
		/// </summary>
		public Ease.EaseFunctions EaseFunction = Ease.EaseFunctions.Linear;

		/// <summary>
		/// Reference to the renderer.
		/// </summary>
		private Renderer r;
		/// <summary>
		/// The colors of the materials.
		/// </summary>
		private Color[] colors;

		protected override void InitEffect() {
			// Get renderer
			r = GetComponent<Renderer>();

			// Get all colors of the materials
			colors = new Color[r.materials.Length];
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i] = r.materials[i].color;
			}
		}

		protected override void UpdateEffect() {
			// Get the current alpha value
			float a;
			if (Start > End) a = Start - Ease.GetEase(EaseFunction, currentTime / Duration, End, Start);
			else a = Ease.GetEase(EaseFunction, currentTime / Duration, Start, End);

			// Apply the alpha value
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i].a = a;
				r.materials[i].color = colors[i];
			}
		}

		protected override void CleanUpEffect() {
			// Set all alpha values to the end value
			for (int i = 0; i < r.materials.Length; i++) {
				colors[i].a = End;
				r.materials[i].color = colors[i];
			}
		}
	}
}

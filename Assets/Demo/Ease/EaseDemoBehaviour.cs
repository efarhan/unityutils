using UnityEngine;
using Utils;

/// <summary>
/// Base class for curve and equation based demo behaviours.
/// </summary>
public abstract class EaseDemoBehaviour : MonoBehaviour {

	[Tooltip("Whether to apply the value to the position or not.")]
	/// <summary>
	/// Whether to apply the value to the position or not.
	/// </summary>
	public static bool ApplyToPosition = true;
	[Tooltip("Wether to apply the value to the scaling or not.")]
	/// <summary>
	/// Wether to apply the value to the scaling or not.
	/// </summary>
	public static bool ApplyToScaling = true;
	[Tooltip("Whether to apply the value to the rotation or not.")]
	/// <summary>
	/// Whether to apply the value to the rotation or not.
	/// </summary>
	public static bool ApplyToRotation = true;
	[Tooltip("Whether to loop or not.")]
	/// <summary>
	/// Whether to loop or not.
	/// </summary>
	public static bool Loop = false;

	/// <summary>
	/// The speed of the ease.
	/// </summary>
	public float Speed = 0.5f;
	/// <summary>
	/// The distance of the ease.
	/// </summary>
	public float Distance = 1f;

	/// <summary>
	/// The original position.
	/// </summary>
	protected Vector3 originalPosition;
	/// <summary>
	/// The original rotation.
	/// </summary>
	protected Quaternion originalRotation;
	/// <summary>
	/// The fraction.
	/// </summary>
	private float t = 0f;
	/// <summary>
	/// Wheter to animate or not animate. Used for mouse down behaviour.
	/// </summary>
	private bool animate = true;

	void Start () {
		// Get original position
		originalPosition = transform.position;
	}

	void Update () {
		// Handle if update is necessairy
		if (Loop) animate = true;
		if (!animate) return;

		// Handle timing
		t += Time.deltaTime * Speed;
		if (t > 1f) {
			if (!Loop) animate = false;
			t = 0f;
		}

		// Call functions if necessairy
		if (ApplyToPosition) TransformPosition(t);
		if (ApplyToScaling) TransformScaling(t);
		if (ApplyToRotation) TransformRotation(t);
	}

	/// <summary>
	/// Prototype for transforming the position.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected abstract void TransformPosition(float t);

	/// <summary>
	/// Prototype for transforming the scaling.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected abstract void TransformScaling(float t);

	/// <summary>
	/// Prototype for transforming the rotation.
	/// </summary>
	/// <param name="t">The fraction.</param>
	protected abstract void TransformRotation(float t);
	
	void OnMouseDown () {
		// Start the animation over if the game object is clicked
		animate = true;
		t = 0f;
	}
}
